# GMRA #
Geomteric Multiresolution Analysis implementation in C++ with an R package front end.

## R Package Installation ##

The package can be installed through CMake by running

*  make R_gmra_install (after running cmake)

Pre-packaged versions are avaialble from the download page:

*  https://bitbucket.org/suppechasper/gmra/downloads/
